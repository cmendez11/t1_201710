package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Number> iter = bag.getIterator();
			while(iter.hasNext()){
				mean +=(Double) iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public Double getMax(IntegersBag bag){
		Double max = Double.MIN_VALUE;
		Double value;
		if(bag != null){
			Iterator<Number> iter = bag.getIterator();
			while(iter.hasNext()){
				value =(Double) iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	public Double getMin(IntegersBag bag){
		
		Double max = Double.MAX_VALUE;
		Double value;
	    int x=0;
		if(bag != null){
			Iterator<Number> iter = bag.getIterator();
			while(iter.hasNext()){
				x++;
				value =(Double) iter.next();
				if( max > value){
					max = value;
				}
			}
			
			
		}
		if(x==0)
		{
			
		}
		return max;
	}
	public double suma(IntegersBag bag){
		
	    double value=0;
	    
		if(bag != null){
			Iterator<Number> iter = bag.getIterator();
			while(iter.hasNext()){
				value += (Double)iter.next();
				
			}
			
		}
		return value;
	}
public Double multPxU(IntegersBag bag){
		
	   
	
		return getMax(bag)*getMin(bag);
	}
	
}
